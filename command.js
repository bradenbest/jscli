class Command {
    static COMMANDS = {
        "help":         Command.cmd_help,
        "clear":        Command.cmd_clear,
        "autocomplete": Command.cmd_autocomplete,
        "history":      Command.cmd_history,
    };
    static COMMAND_HELP = {
        "help": [
            "syntax: help [command]",
            "shows help on [command], or general help if no command is provided",
            "arguments labeled [argument] are optional",
            "arguments labeled {argument} are required",
        ],
        "clear": [
            "syntax: clear",
            "clears the console",
        ],
        "autocomplete": [
            "syntax: autocomplete [action]",
            "manages autocomplete list. Default action is show",
            "",
            "actions:",
            "  show          show list",
            "  reset         reset list to defaults",
            "  add {value}   add <value> to end of list",
            "  remove {idx}  remove element at index <idx>",
        ],
        "history": [
            "syntax: history [action]",
            "manages history list. Default action is show",
            "",
            "actions:",
            "  show   show list",
            "  clear  clear list",
        ],
    };
    static HELP_BASIC = [
        "Command list:",
        "  help          show help (hint: try 'help help')",
        "  clear         clears the console",
        "  autocomplete  manage autocomplete list",
        "  history       manage history list",
        " ",
        "Keybinds:",
        "  Enter   send command",
        "  Up      go back in command history",
        "  Down    go forward in command history",
        "  Tab     autocomplete last argument",
    ];
    static AUTOCOMPLETE_WORDS_DEFAULT = [
        "help",
        "clear",
        "autocomplete", "show", "reset", "add", "remove",
        "history", "show", "clear",
    ];

    static cmd_help(args) {
        if (args.length === 1 || !(args[1] in Command.COMMANDS)) {
            Console.log(Command.HELP_BASIC);
            return;
        }

        Console.log(Command.COMMAND_HELP[args[1]]);
    }

    static cmd_clear(args) {
        Console.OUT.innerHTML = "";
        Console.log("Console cleared", "dark");
    }

    static cmd_autocomplete(args) {
        if (args.length === 1)
            args.push("show");

        switch (args[1]) {
            default: {
                Console.log(`Unrecognized action '${args[1]}'`, "dark");
                break;
            }

            case "show": {
                Console.log("Autocomplete list:");
                Console.log(Console.autocomplete_words.map((str, idx) => `  ${idx}: ${str}`));
                break;
            }

            case "reset": {
                Console.reset_autocomplete_words();
                Console.log("autocomplete list reset", "dark");
                break;
            }

            case "add": {
                let value = args[2];

                if (typeof value === "undefined") {
                    Console.log("Missing value. see 'help autocomplete'.", "dark");
                    return;
                }

                Console.autocomplete_words.push(value);
                break;
            }

            case "remove": {
                let idx = args[2];

                if (typeof idx === "undefined") {
                    Console.log("Missing index. see 'help autocomplete'.", "dark");
                    return;
                }

                Console.autocomplete_words = Util.array_remove(Console.autocomplete_words, idx|0);
                break;
            }
        }
    }

    static cmd_history(args) {
        if (args.length === 1)
            args.push("show");

        switch (args[1]) {
            default: {
                Console.log(`Unrecognized action '${args[1]}'`, "dark");
                break;
            }

            case "show": {
                Console.log("History:");
                Console.log(Console.history.map((str, idx) => `  ${idx}: ${str}`));
                break;
            }

            case "clear": {
                Console.reset_history();
                Console.log("History cleared", "dark");
                break;
            }
        }
    }
}
