class Main {
    static VERSION = "1.1";

    static main() {
        Console.init();
        console.log(`JSCLI v${Main.VERSION}`)
    }
}

window.addEventListener("load", Main.main);
