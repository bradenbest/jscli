class Console {
    static CONTAINER;
    static IN;
    static OUT;

    static autocomplete_words;
    static history;
    static history_fw;

    static init() {
        Console.CONTAINER = document.querySelector(".container");
        Console.IN = document.querySelector(".console.in");
        Console.OUT = document.querySelector(".console.out");
        Console.reset_autocomplete_words();
        Console.reset_history();

        Console.CONTAINER.tabIndex = 0;
        Console.OUT.tabIndex = 0;

        Console.CONTAINER.addEventListener("keydown", Console.ev_keydown_extern);
        Console.OUT.addEventListener("keydown", Console.ev_keydown_extern);
        Console.IN.addEventListener("keydown", Console.ev_keydown);
    }

    static reset_history() {
        Console.history = [];
        Console.history_fw = [];
    }

    static reset_autocomplete_words() {
        let copy = Command.AUTOCOMPLETE_WORDS_DEFAULT.map(v => v);
        Console.autocomplete_words = [...new Set(copy)];
    }

    static autocomplete_unique() {
        Console.autocomplete_words = [...new Set(Console.autocomplete_words)];
    }

    /* text can be one of 3 types:
     * - DOM Element
     * - array<string>
     * - string
     *
     * DOM Elements are injected directly. arrays and strings are dumped
     * into a <pre> and then injected. DOM Elements allow more than just
     * text to be pushed, for example, HTML tables and images.
     */
    static log(text, ...additional_classes) {
        let node = document.createElement("div");
        let textnode = document.createElement("pre");

        if (text instanceof HTMLElement)
            node.appendChild(text);
        else
            node.appendChild(textnode);

        if (Array.isArray(text))
            textnode.innerHTML = text.join("\n");
        else
            textnode.innerHTML = text;

        node.className = "console_log";

        additional_classes.forEach(cls => {
            node.className += ` ${cls}`;
        });

        Console.OUT.appendChild(node);
        Console.OUT.scrollTo(0, Console.OUT.scrollHeight);
    }

    static ev_keydown(ev) {
        const mappings = {
            "Enter":     "submit",
            "ArrowUp":   "history_back",
            "ArrowDown": "history_forward",
            "Tab":       "autocomplete",
        };

        if (ev.key in mappings) {
            ev.preventDefault();
            Console[mappings[ev.key]]();
        }
    }

    static ev_keydown_extern(ev) {
        Console.IN.focus();
    }

    static submit() {
        let line = Console.IN.value;

        if (line === "")
            return;

        Console.IN.value = "";
        Console.history.push(line);

        while (Console.history_fw.length > 0)
            Console.history.push(Console.history_fw.pop());

        Console.try_command(line);
    }

    static history_back() {
        let value = Console.history.pop();

        if (typeof value === "undefined") {
            Console.log("Reached top of history", "dark");
            return;
        }

        if (Console.IN.value !== "")
            Console.history_fw.push(Console.IN.value);

        Console.IN.value = value;
    }

    static history_forward() {
        let value = Console.history_fw.pop();

        if (Console.IN.value !== "")
            Console.history.push(Console.IN.value);

        if (typeof value === "undefined") {
            Console.IN.value = "";
            Console.log("Reached bottom of history", "dark");
            return;
        }

        Console.IN.value = value;
    }

    static autocomplete() {
        let args = Console.IN.value.split(" ");
        let lastarg = args.pop();
        let matches = Console.autocomplete_words.filter(word => word.match(lastarg) !== null);

        if (matches.length === 0)
            return;

        if (matches.length === 1) {
            args.push(matches[0]);
            Console.IN.value = args.join(" ");
            return;
        }

        Console.log("multiple matches found:", "dark");
        Console.log(matches);
    }

    static try_command(line) {
        let args = line.split(" ");
        let cmdfn = Command.COMMANDS[args[0]];

        Console.log(`command: [${args.join(", ")}]`, "dark");

        if (typeof cmdfn === "undefined") {
            Console.log(`command '${args[0]}' not found.`);
            return;
        }

        cmdfn(args);
    }
}

