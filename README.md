# JSCLI - JavaScript Command Line Interface

A simple template/base for building a CLI app in javascript. Comes with QoL features like command history (up, down) and
autocomplete (tab).

The CLI is less a shell and more a REPL. There is no command language other than linear tokenization using space as the
field separator.

## Usage

Simply edit command.js

```js
static COMMANDS = {
    "command": function(args) {
        // args[0] = name of command
        // args[1...] = command arguments

        // Use Console.log (not to be confused with console.log) to output to console
        // Console.log(output, ...additional_classes)
        // `output` can be string, array<string> or HTML Element
        // strings are wrapped in <pre>, HTML Elements are injected directly.
        // `additional_classes` is CSS classes that will be added on.
        // Example: Console.log("the thing has been done", "dark");
    },
    ...
};
static COMMAND_HELP = {
    "command": [
        "syntax: command [argument]",
        "description of arguments",
    ],
    ...
};
static HELP_BASIC = [
    "Command list:",
    "  command  describe your command",
    ...
];
static AUTOCOMPLETE_WORDS_DEFAULT = [
    "command", "argument1", "argumen2", ...
    ...
];
```

The file comes with these commands by default: help, clear, history, autocomplete.
